@extends('layout')

@section('content')
@include('error')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>
                {{$task->tittle}}
            </h3>
            <p>
                {{$task->description}}
            </p>
        </div>
    </div>
</div>
