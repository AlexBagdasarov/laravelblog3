@extends('layout')

@section('content')
@include('error')
<div class="container">
    <h3>Create Task</h3>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => ['tasks.store']]) !!}
            <div class="form-group">
                <input type="text" class="form-control" name="tittle" value="{{old('tittle')}}">
                <br>
                <textarea name="description" id="" cols="30" rows="10" class="form-control" value="{{old('description')}}"></textarea>
                <br>
                <button class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>